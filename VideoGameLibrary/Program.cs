﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

//new class declared
public class VideoGame
{
    //Create a list - part of the class VideoGame
    public static List<VideoGame> library = new List<VideoGame>();

    //public string
    public string GameName { get; set; }

    //public double
    public double GameId { get; set; }

    //public double
    public static double counter;

    public static void addGameToList(string gameName)
    {
        Console.WriteLine("You have entered - " + gameName);
        Console.WriteLine("Adding game to list!");
        counter = 0;
        foreach (VideoGame game in library)
        {
            counter += 1;
        }

        library.Add(new VideoGame { GameName = gameName, GameId = counter });
    }

    public static void displayList()
    {
        //Display the games in the list.
        if ((library != null) && (!library.Any()))
        {
            Console.WriteLine("The list is empty! Please add a game to the list!");
            Console.WriteLine("Press enter to go back to the main menu");
            Console.ReadLine();
        }
        else
        {
            foreach (VideoGame game in library)
            {
                Console.WriteLine(game);
            }
            Console.WriteLine("Press enter to go back to the main menu");
            Console.ReadLine();

        }
    }

    public static void printToFile()
    {
        bool printLoop = true;
        while (printLoop == true)
        {
            Console.WriteLine("Please enter the path to where you want the file to be written to:");
            string path = Console.ReadLine();
            path = path + "\\library.txt";
            if (File.Exists(path))
            {
                Console.WriteLine("This file already exists! Please try again!");
            }
            else
            {
                try
                {
                    string gameString = "";
                    foreach (VideoGame game in VideoGame.library)
                    {
                        gameString += game + "\n";
                    }

                    File.WriteAllText(path, gameString);

                    Console.WriteLine("File written to: " + path);
                    Console.WriteLine("File is called 'library.txt'");
                    printLoop = false;
                    Console.WriteLine("Press enter to return to main menu.");
                    Console.ReadLine();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error! Please try again!");
                    Console.WriteLine(e);
                }
            }
        }
    }

    //this method is used to print items on the list to the screen
    //remember - this override method cannot use the new, static or virtual modifiers 
    //provides a new implementation of a member that is inherited from a base class
    //in this case, we are overriding the default ToString method!
    public override string ToString()
    {
        return "ID: " + GameId + "  Game: " + GameName;
    }
}
public class Example
{
    public static void Main()
    {
        bool loopCheck = true;
        while (loopCheck == true)
        {
            Console.Title = "Video Game List in C#";
            Console.WriteLine("Welcome to the video game list in C#!");
            Console.WriteLine("Please type the first letter of a menu option");
            Console.WriteLine("(V)iew list of games");
            Console.WriteLine("(A)dd game to library");
            Console.WriteLine("(R)emove entry from library");
            Console.WriteLine("(P)rint to file");
            Console.WriteLine("(Q)uit");
            string choice = Console.ReadLine();
            choice = choice.ToUpper();

            switch (choice)
            {
                case "V":
                    VideoGame.displayList();
                    break;

                case "A":
                    //add game to list
                    Console.WriteLine("Please enter the name of the game that you wish to add to the list!");
                    string gameName = Console.ReadLine();
                    VideoGame.addGameToList(gameName);
                    break;

                case "R":
                    Console.WriteLine("Please enter the name of the game you wish to remove");
                    string removeGame = Console.ReadLine();
                    //VideoGame.library.Remove(removeGame);
                    break;

                case "P":
                    VideoGame.printToFile();
                    break;

                case "Q":
                    Console.WriteLine("Press enter to exit application.");
                    loopCheck = false;
                    break;

                default:
                    Console.WriteLine("Error! Please try again!");
                    break;

            }
        }
    }
}